objects are arbitrary objects. In soynet practice, there are three types 
of objects:
 * blobs
   These contain completely arbitrary data. There is absolutely no constraint 
   on their contents. Blobs are only referenced by their hashes, which are 
   also used as the names of the files on disk.
 * records
   These are objects whose contents contain data that points to a blob. In 
   the typical use case in soynet, this is the metadata of a blob, such as 
   its Content Type, Record ID, Transaction ID, and a pointer to its parent.
 * transactions
   These are objects that contain references to all the records that make up 
   a particular transaction.  The Transaction ID is included in this object, 
   and the transaction is INVALID if any of its child records have a 
   Transaction ID different from this one.  The Transaction ID is duplicated 
   in this way in order to implement record-level MVCC.

objects follow the same structure as git objects: a header, followed by a 
NUL character (\0), followed by the content.  The hash of the file is computed 
on the combination of the header and the content.  The whole object (header 
plus content) is deflated using zlib before it is stored on disk.

The object header is defined as:
<type><Space><length>

 * <type> is one of the following strings (excluding quotes):
   - 'blob'
   - 'record'
   - 'transaction'
 * <Space> is a literal space character.
 * <length> is the length of the content in bytes.


The data directory contains three subdirectories:
 * objects
   This directory is used to store objects of all types in git fashion: 
   subdirectories of the first 2 characters of the sha-1 hash, containing 
   filenames of the reamaining 38 characters of the sha-1 hash. An object is
   invalid if the sha-1 of the data is not the same as the concatenation of 
   the foldername and the filename.  After the hash is computed, the data is 
   compressed with zlib, and that compressed data is stored to disk.
 * records
   This directory contains an id-indexed view of all records. A file in this 
   directory contains a 40 byte hash of a record object, then a newline.
   If the file is not exactly 41 bytes, it is considered invalid. Additionally,
   no records may be read while there are transactions being processed, and if 
   a transaction starts processing while a read is happening, the read must 
   detect and abort. This can be detected by checking the timestamp on the 
   transactions/proc directory.
   The directory is organized so that each record has a folder, containing 
   at least one file each.  The folder name should be the Record ID, and 
   names of the files should be the Transaction ID of each.  Since each 
   Transaction ID should be globally unique, there should be no conflicts.
 * transactions
   This is a directory for keeping the state of all transactions.  This 
   directory contains three directories:
    * new
    * proc
    * cur
   The workflow is like this:
    1. The transaction data is written to an object.
    2. The a file, named the same as the Transaction ID, is written in new. 
       This file contains only a hash of the transaction object.
    3. When a processing daemon sees new transactions, it takes the 
       transaction with the lowest Transaction ID, and moves it to the 
       proc directory, where it will remain while it processes the transaction.
    4. Processing the Transaction
       a. Write all object data to the objects folder.
       b. Write all files to the records folder.
    5. When the transaction is finished processing, the daemon will link the 
       file into the cur directory, then remove the file pointer from the proc 
       directory.
    A compaction process may run later to remove/compress the processed 
    transactions, but if not these files will remain indefinitely.

Error recovery is done automatically on read for data accuracy (that what 
went in is the same as what comes out). However, disk errors may occur, or 
the server could be shut down in the middle of a write. To address issues 
that this causes, the following precautions must be taken:

 1. When data is requested, the read worker must first check that there are 
    no files in the transactions/proc directory, and note the timestamp. After 
    the data has been retrieved, it must then check the timestamp on the 
    transactions/proc directory, to ensure that no data was written while it 
    was retrieving the data, which could cause transactions to be partially 
    applied in the data returned.
 2. When the daemon/application is started, the process must do a startup 
    check in the directory to see if any transactions were in process when 
    the server halted. If so, these must be handled first before any other 
    database activity can happen.

In order to support dicts and sets natively in the database, so as to allow 
writers to add/remove members from them without needing complete knowledge of 
all current members, there are three types of records: blob, set, and dict. 
 * blob
   A blob record is a standard record. It has a pointer to a blob.
 * del
   A del record has no sha1 pointer to data (except for the parent). It marks 
   a record that previously existed as explicity deleted.
 * dict
   A dict record has no direct pointer to a blob object. Instead, when a dict 
   record is read, the reader must look for records starting with the same 
   name, plus a hierarchy separator ('/') followed by a period ('.') and the 
   string key of the dict item. Keys may not contain the hierarchy separator. 
   It is recommended that records not involved with dicts not use Record IDs 
   that contain the hierarchy separator immediately followed with a period, 
   to avoid confusion with these dicts. With this definition, native multi-
   level dicts are possible, if perhaps not recommended.
 * set
   A set record is a dict record, where the string keys are defined to be the 
   sha1 hash of the blob that the dict item points to.

A Transaction should be held in the transactions/tmp directory until all 
objects specified in the transaction and associated links have been properly 
stored in the objects folder. After a server failure, and upon startup, the 
transactions/tmp directory should be purged, as they are incomplete 
transactions, and will not have been reported as successful to the client. 
Transactions in the tmp directory should be named with a uuid4, as with the 
process of writing objects. This ensures that if two processes somehow are 
processing the same incoming transaction, such as two processes each handling 
a replication operation from two other databases in the cluster, that each 
process' work won't affect the other's.

An object file is assumed to be proper if it exists in the correct folder with 
an appropriate name, and upon read, is verified that the hash is correct.

A record or transaction file is assumed correct if it exists and its size is 
exactly 41 bytes (40 bytes for the hex representation of the sha1, plus the
trailing newline).

A valid Record ID may use any safe URL characters. In particular, the '/' 
character is encouraged as a namespace delimiter for records.

A Transaction ID is of the form <timestamp>-<uuid>.

<timestamp> is the decimal representation of the number of milliseconds since 
the unix epoch, padded to 15 characters. In python: 
str(int(round(time.time()))).zfill(15) 

<uuid> is an rfc4122 uuid4, to ensure beyond reasonable doubt that each 
Transaction ID is unique.  Because this ID is so reliant on the timestamp, it 
is imperative that the nodes of the cluster have their time syncronized as 
accurately as possible.

An example Transaction ID then is:
001321177396109-552734c4-f2ad-4fa3-8564-6b7cb31ef9ce

which could have been generated by the python statement:
>>> str(int(round(time.time() * 1000))).zfill(15) + '-' + str(uuid.uuid4())

A blob object's payload is any arbitray set of bytes, so a description of it 
is unnecessary.

A transaction object's payload is a newline '\n' separated list of the hashes 
of the record objects contained in the transaction, followed by the standard 
trailing newline, typically added transparently by text editors. There is no 
need for additional complexity, as a transaction is fundamentally just a set 
of records that should all be changed simultaneously.

**SHOULD THE TRANSACTION ITSELF BE SIGNED**

A record object's payload is as follows:
<Record ID>
<Transaction ID>
<record type> (<sha1 hash>|-) [<parent sha1 hash>]
[<Headers>]

Example Record Object Payload:
soynet/eth/0123456789ab
001321177396109-552734c4-f2ad-4fa3-8564-6b7cb31ef9ce
blob 44e96b8194086d804744acba2293ac0020d81b63 02135ff9133db03a40aaff586bb173e6a7a6998f
Content-Type: application/json

Second Example Record Object Payload:
soynet/nodes/abc123
001321177396109-552734c4-f2ad-4fa3-8564-6b7cb31ef9ce
del - 02135ff9133db03a40aaff586bb173e6a7a6998f
