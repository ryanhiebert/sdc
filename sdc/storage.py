import zlib, hashlib, os, os.path, errno, uuid
from .config import config

DATA_DIR = config.main.data_dir
BASE_DIRS = (
             'transactions/tmp',
             'transactions/new',
             'transactions/cur',
            )
# Make sure the BASE_DIRS exist
for dirs in BASE_DIRS:
    path = os.path.join(DATA_DIR, dirs)
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

class Object(bytes):
    """
    A bytes object that knows how to store itself, and fact-check itself.
    """
    
    def __new__(cls, source=None, type=None, identity=None):
        """
        If source is present, fill with the source, checking whether it is 
        already a instance of Object. Identity is checked if present.
        If source is not present, but filename is, try retrieving the object 
        with the given identity. In both cases, if type is present, the type 
        is confirmed, with a ValueError thrown if it is not the correct type.
        """
        if source is None and identity is None:
            raise TypeError("Either source or identity must be present")
        elif source is not None:
            try:
                if ((source.type is type or type is None) and
                    (identity is None or 
                     cls.sha1(cls, source).hexdigest() is identity)):
                    return bytes.__new__(cls, source)
                else:
                    raise ValueError('Object type is not {0}'.format(type))
            except AttributeError:
                if type is None:
                    raise ValueError('Type is required to create new Object')
                else:
                    header = cls.make_header(source, type)
                    return bytes.__new__(cls, b'\0'.join([header, source]))
        else:
            with open(cls.path_from_hash(identity), 'rb') as f:
                databytes = zlib.decompress(f.read())
            if (hashlib.sha1(databytes).hexdigest() is not identity or
                cls.type(databytes) is not type):
                    os.remove(cls.path_from_hash(identity))
                    raise ValueError('Source file corrupt')
            else:
                return bytes.__new__(cls, databytes)

    @staticmethod
    def make_header(data, type):
        type = type
        length = str(len(data))
        return bytes(bytearray(' '.join([type, length]), 'utf-8'))

    @staticmethod
    def path_from_hash(hash):
        bin, file = hash[:2], hash[2:]
        return os.path.join(DATA_DIR, 'objects', bin, file)

    @property
    def sha1(self):
        return hashlib.sha1(self)

    @property
    def header(self):
        return self.split(b'\0', 1)[0]

    @property
    def type(self):
        return self.header.split(b' ', 1)[0]

    @property
    def size(self):
        return int(self.header.split(b' ', 1)[1])

    @property
    def data(self):
        return self.split(b'\0', 1)[1]

    def __repr__(self):
        return ('sdc.storage.Object(' + bytes.__repr__(self.data) + 
                ', {0})'.format(repr(bytearray(self.type).decode('utf-8'))))

    @property
    def saved(self):
        """
        Checks whether this object exists on disk, and validates.
        """
        path = self.path_from_hash(self.sha1.hexdigest())
        if os.path.isfile(path):
            read_data = open(path, 'rb').read()
            zlib_decompress = zlib.decompress(read_data)
            hash = hashlib.sha1(zlib_decompress).hexdigest()
            if hash == self.sha1.hexdigest():
                return True
            else:
                os.remove(path) # File read is corrupt
        return False

    def save(self):
        """
        Save the object. No temp folder needed: object verified on read.
        """
        if not self.saved:
            filename = self.path_from_hash(self.sha1.hexdigest())
            if not os.path.exists(os.path.dirname(filename)):
                try:
                    os.makedirs(os.path.dirname(filename))
                except OSError as e:
                    if e.errno != errno.EEXist:
                        raise
            with open(filename, 'wb') as f:
                f.write(zlib.compress(self))

class BlobObject(Object):
    def __new__(cls, source=None, identity=None):
        return Object.__new__(cls, source, 'blob', identity)

class RecordObject(Object):
    def __new__(cls, source=None, identity=None):
        return Object.__new__(cls, source, 'record', identity) 

    @property
    def record_id(self):
        return self.data.split(b'\n')[0]

    @property
    def transaction_id(self):
        return self.data.split(b'\n')[1]

    @property
    def record_line(self):
        return self.data.split(b'\n')[2]

    @property
    def record_type(self):
        return self.record_line.split(b' ')[0]

    @property
    def blob(self):
        val = self.record_line.split(b' ')[1]
        if val is '-':
            return None
        else:
            return BlobObject(identity=val)

    @property
    def headers(self):
        headers = {}
        for line in self.data.split(b'\n')[3:-1]:
            name, value = line.split(b': ', 1)
            headers[name] = value
        return headers

    @property
    def parent_record(self):
        try:
            return RecordObject(identity=self.record_line.split(b' ')[2])
        except IndexError:
            return None

    def file(self):
        if not self.filed:
            pass #File it in DATA_DIR/records

class TransactionObject(Object):
    def __new__(cls, source=None, identity=None):
        return Object.__new__(cls, source, 'transaction', identity)

    def file(self):
        if not self.filed:
            pass #File it in DATA_DIR/transactions

class MutableObject(bytearray, Object):
    def __new__(cls, source=None, type=None, *args, **kwargs):
        return bytearray.__new__(cls, Object(source, type))

    def __init__(self, source=None, type=None, data=None):
        # source and type have already been accounted for in __new__
        self.data = data

    @Object.header.setter
    def header(self, value):
        start = 0
        end = len(self.header)
        self[start:end] = value

    @Object.type.setter
    def type(self, value):
        parval = self.header # Work with a mutable sequence
        start = 0
        end = len(self.type)
        parval[start:end] = value
        header = parval

    @Object.size.setter
    def size(self, value):
        parval = self.header # Work with a mutable sequence
        start = len(self.type) + 1 # +1 for separator
        end = len(self.size)
        parval[start:end] = bytearray(str(value), 'utf-8')
        header = parval

    @Object.data.setter
    def data(self, value):
        start = len(self.header) + 1 # +1 for \0 sep of header
        end = start + len(self.data)
        self[start:end] = value
        # The data has changed, so the size needs to be updated
        self.size = len(self.data)

class Record(MutableObject, RecordObject):
    def __new__(cls, source=None, *args, **kwargs):
        if source is None:
            source = b'\n\nnil -\n\n' # An empty record
        return MutableObject.__new__(cls, RecordObject(source))

    def __init__(self, source=None, record_id=None, transaction_id=None,
                 record_type=None, blob=None, blob_hash=None,
                 parent_record=None, parent_record_hash=None, headers={}):
        # source has already been accounted for in __new__
        self.record_id = record_id
        self.transaction_id = transaction_id
        self.record_type = record_type
        self.headers = headers

        # Special Cases
        if blob is None and blob_hash is None:
            self.blob = None
        else:
            self.blob = BlobObject(blob, blob_hash)

        if parent_record is None and parent_record_hash is None:
            self.parent_record = None
        else:
            self.parent_record = RecordObject(parent_record,
                                              parent_record_hash)

    @RecordObject.record_id.setter
    def record_id(self, value):
        parval = self.data
        start = 0
        end = len(self.record_id)
        parval[start:end] = value
        self.data = parval

    @RecordObject.transaction_id.setter
    def transaction_id(self, value):
        parval = self.data
        start = len(self.record_id) + 1
        end = start + len(self.transaction_id)
        parval[start:end] = value

    @RecordObject.record_line.setter
    def record_line(self, value):
        parval = self.data
        start = len(self.record_id) + len(self.transaction_id) + 2
        end = start + len(self.record_line)
        parval[start:end] = value

    @RecordObject.record_type.setter
    def record_type(self, value):
        parval = self.record_line
        start = 0
        end = len(self.record_type)
        parval[start:end] = value
        self.record_line = parval

    @RecordObject.blob.setter
    def blob(self, value):
        if value is None:
            value = b'-'
        else:
            value = bytearray(value.sha1, 'utf-8')
        parval = self.record_line
        start = len(self.record_type) + 1
        if self.blob is None:
            end = start + 1 # Length of Placeholder
        else:
            end = start + 40 # Length of Hash
        parval[start:end] = value
        self.record_line = parval

    @RecordObject.parent_record.setter
    def parent_record(self, value):
        parval = self.record_line
        if self.parent_record is None:
            oldparlen = 0
        else:
            oldparlen = len(self.parent_record) + 1 # Includes separator
        if value is None:
            value = b''
        else:
            value = b' ' + bytearray(value.sha1, 'utf-8')
        parval[-oldparlen:] = value
        self.record_line = parval

    @RecordObject.headers.setter
    def headers(self, value):
        # This one may be more difficult, because I'll need to give it a 
        # mutable dict-like that it can update.
        pass

    def freeze(self):
        """
        Returns an immutable RecordObject from this Record.
        """
        if self.record_id is None:
            raise ValueError('record_id must be set')
        elif self.transaction_id is None:
            raise ValueError('transaction_id must be set')
        elif self.record_type is None:
            raise ValueError('record_type must be set')
        elif self.record_type not in ('del','dict','set','blob'):
            raise ValueError('invalid record_type')
        elif self._blob is None and self.record_type is 'blob':
            raise ValueError('This record type requires a blob')
        return RecordObject(self)

class Transaction(object):
    def __init__(self, value):
        try:
            if value.type is 'transaction':
                self.records = value.records
        except AttributeError:
            self.records = value

    def freeze(self, value):
        records = self.records
        records.append('') # Have a trailing newline after it's joined
        return TransactionObject(self.records.join('\n'))
