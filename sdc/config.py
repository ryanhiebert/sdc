"""
This is the config module.  It retrieves config from _CONFIGFILES[]
XXX undocumented
"""

# vim:tabstop=4:softtabstop=4:shiftwidth=4:smarttab:
# vim:expandtab:nosmartindent:autoindent:nowrap:textwidth=0

try:
    import configparser
except ImportError:
    import ConfigParser as configparser
from .support.dotdict import Dotdict

_CONFIGFILES = (
    'sdc.conf',
)

class SDCConfig(configparser.SafeConfigParser):
    """A class for reading SDC Configuration Files"""

    def read(self, f):
        configparser.SafeConfigParser.read(self, f)
        # Set up the dict that implements the iterable. Set like this so that
        # the dict will only be created once, not once per use.
        self._dict = Dotdict()
        for section in self.sections():
            self._dict[section] = Dotdict()
            for option in self.options(section):
                self._dict[section][option] = self.get(section, option)

    def __getitem__(self, section):
        return self._dict[section]

    def __iter__(self):
        return iter(self._dict)

    def __repr__(self):
        return repr(self._dict)

    def keys(self):
        return self._dict.keys()

    def values(self):
        return self._dict.values()

    def items(self):
        return self._dict.items()

    # Allow access to dict items as attributes
    __getattr__ = __getitem__

# Set up the 'config' variable
config = SDCConfig()
config.read(_CONFIGFILES[0])
try:
    for f in _CONFIGFILES[1:]:
        config.read(f)
except configparser.Error:
    pass
