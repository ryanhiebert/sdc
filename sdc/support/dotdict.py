"""Implements a dictionary that allow for access through attributes"""

class Dotdict(dict):
    """A dictionary that gives access by attributes"""
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__
